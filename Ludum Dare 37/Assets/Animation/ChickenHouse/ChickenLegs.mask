%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: ChickenLegs
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Base
    m_Weight: 0
  - m_Path: Base/Root
    m_Weight: 0
  - m_Path: Base/Root/HouseBody
    m_Weight: 0
  - m_Path: Base/Root/HouseBody/Base 1
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Leg.001.L.001
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Leg.001.L.001/Leg.001.L.001_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe1.002
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe1.002/Toe1.001.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe1.002/Toe1.001.L/Toe1.001.L_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe2.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe2.L/Toe2.001.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe2.L/Toe2.001.L/Toe2.001.L_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe3.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe3.L/Toe3.001.L
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.L/Leg.001.L/Toe3.L/Toe3.001.L/Toe3.001.L_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Leg.001.R.001
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Leg.001.R.001/Leg.001.R.001_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe1
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe1/Toe1.001.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe1/Toe1.001.R/Toe1.001.R_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe2.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe2.R/Toe2.001.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe2.R/Toe2.001.R/Toe2.001.R_end
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe3.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe3.R/Toe3.001.R
    m_Weight: 1
  - m_Path: Base/Root/HouseBody/Base 1/Leg.R/Leg.001.R/Toe3.R/Toe3.001.R/Toe3.001.R_end
    m_Weight: 1
  - m_Path: House.001
    m_Weight: 0
