%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Rotation
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Bone
    m_Weight: 1
  - m_Path: Armature/Bone/Base
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody/hand.L
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody/hand.L/hand.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody/hand.R
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody/hand.R/hand.R_end
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody/Head
    m_Weight: 0
  - m_Path: Armature/Bone/Base/UpperBody/Head/Head_end
    m_Weight: 0
  - m_Path: Armature/Bone/Leg.L
    m_Weight: 0
  - m_Path: Armature/Bone/Leg.L/Leg.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Leg.R
    m_Weight: 0
  - m_Path: Armature/Bone/Leg.R/Leg.R_end
    m_Weight: 0
  - m_Path: Player
    m_Weight: 0
