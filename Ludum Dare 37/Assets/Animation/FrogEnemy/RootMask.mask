%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: RootMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Root
    m_Weight: 1
  - m_Path: Armature/Root/Foot.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/InnerToe.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/InnerToe.L/InnerToe.001.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/InnerToe.L/InnerToe.001.L/InnerToe.001.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/MiddleToe.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/MiddleToe.L/MiddleToe.001.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/MiddleToe.L/MiddleToe.001.L/MiddleToe.001.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/OutterToe.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/OutterToe.L/OutterToe.001.L
    m_Weight: 0
  - m_Path: Armature/Root/Foot.L/OutterToe.L/OutterToe.001.L/OutterToe.001.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/InnerToe.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/InnerToe.R/InnerToe.001.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/InnerToe.R/InnerToe.001.R/InnerToe.001.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/MiddleToe.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/MiddleToe.R/MiddleToe.001.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/MiddleToe.R/MiddleToe.001.R/MiddleToe.001.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/OutterToe.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/OutterToe.R/OutterToe.001.R
    m_Weight: 0
  - m_Path: Armature/Root/Foot.R/OutterToe.R/OutterToe.001.R/OutterToe.001.R_end
    m_Weight: 0
  - m_Path: Armature/Root/LowerBody
    m_Weight: 0
  - m_Path: Armature/Root/LowerBody/UpperBody
    m_Weight: 0
  - m_Path: Armature/Root/LowerBody/UpperBody/Eye.L
    m_Weight: 0
  - m_Path: Armature/Root/LowerBody/UpperBody/Eye.L/Eye.L_end
    m_Weight: 0
  - m_Path: Armature/Root/LowerBody/UpperBody/Eye.R
    m_Weight: 0
  - m_Path: Armature/Root/LowerBody/UpperBody/Eye.R/Eye.R_end
    m_Weight: 0
  - m_Path: FrogEnemy
    m_Weight: 0
