﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour {

    [SerializeField]
    int enemySpawnChancePerFrame = 1;

    [SerializeField]
    int oilBarrelSpawnChancePerFrame = 1;

    [SerializeField]
    int minDistanceTohouse = 3;

    [SerializeField]
    int addedValueToMinDistance = 2;

    [SerializeField]
    List<GameObject> enemyPrefabs;

    [SerializeField]
    GameObject oilBarrel;

    [SerializeField]
    GameObject chickenHouse;

    [SerializeField]
    Transform mapBegin;

    [SerializeField]
    Transform mapEnd;

    float spawnRange;

    void Start() {
        //spawnRange = mapEnd.position.x - mapBegin.position.x;
    }
    // Update is called once per frame
    void FixedUpdate() {
        if(Random.Range(1, 1 * enemySpawnChancePerFrame) == Random.Range(1, 1 * enemySpawnChancePerFrame)){
            float possibleSpawn = Random.Range(mapBegin.position.x, mapEnd.position.x);
            if(Mathf.Abs(possibleSpawn - chickenHouse.transform.position.x) >= minDistanceTohouse) {
                if (Random.Range(0, 1) == 0) {
                    Instantiate(enemyPrefabs[(Random.Range(0, enemyPrefabs.Count))], new Vector3(possibleSpawn - addedValueToMinDistance, 1, 0), new Quaternion());
                } else {
                    Instantiate(enemyPrefabs[(Random.Range(0, enemyPrefabs.Count))], new Vector3(possibleSpawn + addedValueToMinDistance, 1, 0), new Quaternion());
                }
            }
        }


        if (Random.Range(1, 1 * oilBarrelSpawnChancePerFrame) == Random.Range(1, 1 * oilBarrelSpawnChancePerFrame)) {
            float possibleSpawn = Random.Range(mapBegin.position.x, mapEnd.position.x);
            if (Mathf.Abs(possibleSpawn - chickenHouse.transform.position.x) >= minDistanceTohouse) {
                if (Random.Range(0, 1) == 0) {
                    Instantiate(oilBarrel, new Vector3(possibleSpawn - addedValueToMinDistance, 1, 0), new Quaternion());
                } else {
                    Instantiate(oilBarrel, new Vector3(possibleSpawn + addedValueToMinDistance, 1, 0), new Quaternion());
                }
            }
        }
    }
}
