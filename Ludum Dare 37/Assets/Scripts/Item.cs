﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD37 {
    public class Item : MonoBehaviour {

        public float ProcessedOilValue {
            get {
                return processedOilValue;
            }

            set {
                processedOilValue = value;
            }
        }

        [SerializeField]
        float processedOilValue = 0;
    }
}
