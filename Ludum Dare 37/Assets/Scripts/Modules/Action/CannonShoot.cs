﻿using LD37;
using LD37.Modules;
using LD37.Modules.Action;
using UnityEngine;

public class CannonShoot : BoolActionModule {

    // Dirtiest hack of my life
    [SerializeField]
    CannonModule cannonModule;
    // I have not lived that long though...

    [SerializeField]
    float cooldown;

    [SerializeField]
    Transform spawn;

    [SerializeField]
    GameObject projectile;

    [SerializeField]
    float force = 10;

    [SerializeField]
    OilTank oilTank;

    [SerializeField]
    float usagePerShot;

    float lastShoot = float.MinValue;

    public CannonShoot() {
        OnAwake += () => {
            if (cannonModule == null) {
                cannonModule = GetComponent<CannonModule>();
            }
        };
    }

    protected override void ValueChange(VetoableValueChangeEvent<bool> changeEvent) {
        if (cannonModule == null || !cannonModule.Disabled) {
            if (changeEvent.NewValue) {
                if (Time.realtimeSinceStartup - lastShoot > cooldown) {
                    Shoot();
                }
            }
        } else {
            changeEvent.Veto();
        }
    }

    void Shoot() {
        if(oilTank.OilLevel - usagePerShot / 100 > 0) {
            GameObject obj = Instantiate(projectile, spawn.position, spawn.rotation);

            obj.GetComponent<Rigidbody2D>().AddForce(obj.transform.up * force);
            obj.AddComponent<AutoDelete>().DestructionTimer = 5;
            oilTank.OilLevel -= usagePerShot / 100;
        } else {
            //PlayStrangeSound
        }
    }
}