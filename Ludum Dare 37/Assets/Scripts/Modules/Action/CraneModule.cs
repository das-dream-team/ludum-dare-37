﻿using LD37.Util;
using UnityEngine;

namespace LD37.Modules.Action {
    public class CraneModule : FloatActionModule {

        private ColliderShare selfShare;

        #region Properties
        public float MinimumDistance {
            get {
                return minimumDistance;
            }

            set {
                minimumDistance = value;
            }
        }

        public float MaximumDistance {
            get {
                return maximumDistance;
            }

            set {
                maximumDistance = value;
            }
        }

        public SpringJoint2D CraneSpring {
            get {
                return craneSpring;
            }

            set {
                craneSpring = value;
            }
        }

        public float CraneDistance {
            get {
                return craneSpring.distance;
            }

            protected set {
                craneSpring.distance = value;
            }
        }

        public float RealDistance {
            get {
                return ((Vector2)CraneSpring.transform.position - (Vector2)CraneSpring.connectedBody.transform.position).magnitude;
            }
        }

        public float LengthRestriction {
            get; private set;
        }

        public float RestrictionSafety {
            get {
                return restrictionSafety;
            }

            set {
                restrictionSafety = value;
            }
        }
        #endregion

        #region Serializeable Fields
        [SerializeField]
        float repairPointsPerSecond = 1;

        [SerializeField]
        float minimumDistance = 1;

        [SerializeField]
        float maximumDistance = 15;

        [SerializeField]
        float restrictionSafety = .25f;

        [SerializeField]
        BoxCollider2D craneArm;

        [SerializeField]
        ColliderShare childShare;

        [SerializeField]
        GrabArm grabArm;

        [SerializeField]
        SpringJoint2D craneSpring;

        [SerializeField]
        ParticleSystem darkSmoke;

        [SerializeField]
        ParticleSystem sparks;

        #endregion

        int enteredColliders;

        int startHealth;

        bool enteredRepairTrigger;

        float repairPointsPerUpdateFiller = 0;

        protected override void ValueChange(VetoableValueChangeEvent<float> changeEvent) {
            if (!Disabled) {
                if (changeEvent.NewValue > changeEvent.OldValue) {
                    if (!float.IsInfinity(LengthRestriction)) {
                        changeEvent.Veto();
                        return;
                    }
                }
            } else {
                changeEvent.Veto();
            }

            SetCrane(changeEvent.NewValue);
        }

        public CraneModule() {
            LengthRestriction = float.PositiveInfinity;

            OnAwake += AwakeHandler;
            OnUpdate += UpdateHandler;
            OnFixedUpdate += FixedUpdateHandler;
            OnHealthChange += HealthChangeHandler;
        }

        void AwakeHandler() {
            selfShare = GetComponent<ColliderShare>();
            selfShare.EventTriggerEnter += TriggerEnterHandler;
            selfShare.EventTriggerExit += TriggerExitHandler;
            SetCrane(0);
            startHealth = Health;
            sparks.Stop();
            darkSmoke.Stop();
        }

        private void UpdateHandler() {
            if (grabArm.GrabTarget == null) {
                if (!GetComponent<StatusLampHandler>().IsRed)
                    GetComponent<StatusLampHandler>().SetLampIdle();
            }

            if (enteredRepairTrigger) {
                if (UnityEngine.Input.GetKey(KeyCode.R)) {
                    repairPointsPerUpdateFiller += repairPointsPerSecond * Time.deltaTime;

                    if (repairPointsPerUpdateFiller > 1) {
                        if (Health < startHealth) {
                            if (Health + repairPointsPerUpdateFiller > startHealth) {
                                Health = startHealth;
                            } else {
                                ChangeHealth((int) repairPointsPerUpdateFiller);
                            }
                        }
                        repairPointsPerUpdateFiller = 0;
                    }
                }
            }
        }

        void FixedUpdateHandler() {
            // safety off

            Vector2 lossyScale = craneArm.transform.lossyScale;

            Vector2 origin = (Vector2) CraneSpring.transform.position + new Vector2(craneArm.offset.x * lossyScale.x, craneArm.offset.y * lossyScale.y);
            Vector2 size = new Vector2(craneArm.size.x * lossyScale.x, craneArm.size.y * lossyScale.y);
            float angle = CraneSpring.transform.rotation.eulerAngles.z;
            Vector2 direction = Vector2.down;
            float distance = RestrictionSafety;

            RaycastHit2D hit = Physics2D.BoxCast(origin, size, angle, direction, distance);
            if (hit.collider == null) {
                LengthRestriction = float.PositiveInfinity;
            } else {
                LengthRestriction = CraneDistance;
            }


            // can grab

            distance = grabArm.GrabDistance;

            hit = Physics2D.BoxCast(origin, size, angle, direction, distance);
            if (hit.collider != null) {
                GetComponent<StatusLampHandler>().SetLampOK();
            }
        }

        private void HealthChangeHandler(int previous, int current, int change) {
            if (current < 0) {
                Health = 0;
            }

            if (current != 0) {
                if (current < startHealth * 0.9) {
                    if (current < startHealth * 0.3)
                        sparks.Play();
                    darkSmoke.Play();
                    float healthPercentage = (float) current / startHealth;
                    darkSmoke.startColor = new Color(77 / 255f, 77 / 255f, 77 / 255f, 1 - healthPercentage);
                } else {
                    sparks.Stop();
                    darkSmoke.Stop();
                }
            }
        }

        void SetCrane(float amount) {
            float value = MinimumDistance + (MaximumDistance - MinimumDistance) * amount;
            float min = MinimumDistance;
            float max = Mathf.Min(MaximumDistance, LengthRestriction);

            CraneDistance = Mathf.Clamp(value, min, max);
        }
        
        private void TriggerEnterHandler(Collider2D collision) {
            if (collision.gameObject.tag.Equals("Player")) {
                enteredRepairTrigger = true;
            }
        }

        private void TriggerExitHandler(Collider2D collision) {
            if (collision.gameObject.tag.Equals("Player")) {
                enteredRepairTrigger = false;
            }
        }

        void OnDrawGizmos() {
            if (Health < startHealth && !Disabled) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(transform.position, 0.4f);
            } else if (Disabled) {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(transform.position, 0.4f);
            }
        }
    }
}
