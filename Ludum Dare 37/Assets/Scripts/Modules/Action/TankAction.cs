﻿using UnityEngine;
using UnityEngine.Events;

namespace LD37.Modules.Action {
    public class TankAction : FloatActionModule {

        [SerializeField]
        GameObject tankTop;

        [SerializeField]
        GameObject tankBottom;

        [SerializeField]
        float oilLevel = 1;

        #region Properties

        public float OilLevel {
            get {
                return oilLevel;
            }

            set {
                oilLevel = value;
            }
        }
        #endregion

        float lastValue;
        float totalValue;

        void Start() {
            totalValue = tankTop.transform.localPosition.y - tankBottom.transform.localPosition.y;
            //tankTop.transform.localPosition = new Vector2(tankTop.transform.localPosition.x, totalValue * OilLevel);
            tankTop.transform.localPosition = new Vector2(tankTop.transform.localPosition.x, 0);
        }

        protected override void ValueChange(VetoableValueChangeEvent<float> changeEvent) {
            tankTop.transform.localPosition = new Vector2(tankTop.transform.localPosition.x, totalValue * changeEvent.NewValue);
        }
    }
}