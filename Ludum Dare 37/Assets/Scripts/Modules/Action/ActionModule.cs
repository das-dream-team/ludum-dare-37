﻿namespace LD37.Modules.Action {
    public abstract class ActionModule<T> : ValueModule<T> {

        public ActionModule() {
            OnChange += ValueChange;
        }

        protected abstract void ValueChange(VetoableValueChangeEvent<T> valueChangeEvent);

    }

    public abstract class BoolActionModule : ActionModule<bool> { }

    public abstract class FloatActionModule : ActionModule<float> { }

    public abstract class IntActionModule : ActionModule<int> { }
}
