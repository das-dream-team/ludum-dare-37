﻿using LD37.Util;
using UnityEngine;

namespace LD37.Modules.Action {
    [RequireComponent(typeof(BoxCollider2D))]
    class ConveyorModule : BoolActionModule {

        private new BoxCollider2D collider;
        private ColliderShare colliderShare;
        
        private float length;

        private bool retract;
        private bool expand;

        int startHealth;

        bool enteredRepairTrigger;

        float repairPointsPerUpdateFiller = 0;

        #region Properties
        public Renderer Model {
            get {
                return model;
            }

            set {
                model = value;
            }
        }

        public float RetractionSpeed {
            get {
                return retractionSpeed;
            }

            set {
                retractionSpeed = value;
            }
        }

        public float ExpansionSpeed {
            get {
                return expansionSpeed;
            }

            set {
                expansionSpeed = value;
            }
        }

        public float MinLength {
            get {
                return minLength;
            }

            set {
                minLength = value;
            }
        }

        public float MaxLength {
            get {
                return maxLength;
            }

            set {
                maxLength = value;
            }
        }

        #endregion

        #region Serialized Fields

        [SerializeField]
        Transform end;

        [SerializeField]
        float repairPointsPerSecond = 1;

        [SerializeField]
        ParticleSystem darkSmoke;

        [SerializeField]
        ParticleSystem sparks;

        [SerializeField]
        private Renderer model;

        [SerializeField]
        private Rigidbody2D house;

        [SerializeField]
        private float retractionSpeed = 2f;

        [SerializeField]
        private float expansionSpeed = 2f;

        [SerializeField]
        private float minLength = 0.5f;

        [SerializeField]
        private float maxLength = 2.5f;

        #endregion

        protected override void ValueChange(VetoableValueChangeEvent<bool> changeEvent) {
            if (!Disabled) {
                if (changeEvent.NewValue) {
                    Toggle();
                }
            } else {
                changeEvent.Veto();
            }
        }

        public ConveyorModule() {
            OnAwake += AwakeHandler;
            OnStart += StartHandler;
            OnUpdate += UpdateHandler;
            OnHealthChange += HealthChangeHandler;
        }

        private void AwakeHandler() {
            collider = GetComponent<BoxCollider2D>();
            colliderShare = GetComponent<ColliderShare>();
        }

        private void StartHandler() {
            colliderShare.EventTriggerEnter += TriggerEnterHandler;
            colliderShare.EventTriggerExit += TriggerExitHandler;
            colliderShare.EventCollisionStay += CollisionStayHandler;
            retract = true;
            expand = false;
            startHealth = Health;
            sparks.Stop();
            darkSmoke.Stop();
        }

        private void UpdateHandler() {
            length = Mathf.Abs(transform.GetChild(0).position.x - end.position.x);

            if (retract) {
                if (length > MinLength) {
                    end.Translate(Vector2.down * Time.deltaTime * RetractionSpeed);
                }
            } else if (expand) {
                if (length < MaxLength) {
                    end.Translate(Vector2.up * Time.deltaTime * ExpansionSpeed);
                }
            }
            collider.offset = new Vector2(length / 2 + 0.125f, 0);
            collider.size = new Vector2(length + 0.25f, collider.bounds.size.y);

            if (enteredRepairTrigger) {
                if (UnityEngine.Input.GetKey(KeyCode.R)) {
                    repairPointsPerUpdateFiller += repairPointsPerSecond * Time.deltaTime;

                    if (repairPointsPerUpdateFiller > 1) {
                        if (Health < startHealth) {
                            if (Health + repairPointsPerUpdateFiller > startHealth) {
                                Health = startHealth;
                            } else {
                                ChangeHealth((int)repairPointsPerUpdateFiller);
                            }
                        }
                        repairPointsPerUpdateFiller = 0;
                    }
                }
            }
        }

        private void HealthChangeHandler(int previous, int current, int change) {
            if (current < 0) Health = 0;

            if (current != 0) {
                if (current < startHealth * 0.9) {
                    if (current < startHealth * 0.3)
                        sparks.Play();
                    darkSmoke.Play();
                    float healthPercentage = (float) current / startHealth;
                    darkSmoke.startColor = new Color(77 / 255f, 77 / 255f, 77 / 255f, 1 - healthPercentage);
                } else {
                    sparks.Stop();
                    darkSmoke.Stop();
                }
            }
        }

        private void TriggerEnterHandler(Collider2D collision) {
            if (collision.gameObject.tag.Equals("Player")) {
                enteredRepairTrigger = true;
            }
        }

        private void TriggerExitHandler(Collider2D collision) {
            if (collision.gameObject.tag.Equals("Player")) {
                enteredRepairTrigger = false;
            }
        }

        private void CollisionStayHandler(Collision2D collision) {
            Rigidbody2D other = collision.collider.GetComponent<Rigidbody2D>();
            if (other != null) {
                other.AddForce(Vector2.left);
            }
        }

        public void Toggle() {
            expand = !expand;
            retract = !retract;
        }

        public void Retract() {
            retract = true;
            expand = false;
        }

        public void Expand() {
            expand = true;
            retract = false;
        }

        void OnDrawGizmos() {
            if (Health < startHealth && !Disabled) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(transform.position, 0.4f);
            } else if (Disabled) {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(transform.position, 0.4f);
            }
        }
    }
}
