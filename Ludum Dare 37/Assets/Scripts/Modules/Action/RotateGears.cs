﻿using UnityEngine;

namespace LD37.Modules.Action {
    public class RotateGears : FloatActionModule {
        protected override void ValueChange(VetoableValueChangeEvent<float> changeEvent) {
            transform.localRotation = Quaternion.Euler(60 * changeEvent.NewValue, transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z);
        }
    }
}
