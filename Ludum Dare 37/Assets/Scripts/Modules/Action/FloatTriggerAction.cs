﻿using UnityEngine;
using UnityEngine.Events;

namespace LD37.Modules.Action {
    public class FloatTriggerAction : FloatActionModule {

        public UnityEvent EventFull {
            get {
                return eventFull;
            }
        }

        public UnityEvent EventEmpty {
            get {
                return eventEmpty;
            }
        }

        [SerializeField]
        UnityEvent eventFull;

        [SerializeField]
        UnityEvent eventEmpty;

        protected override void ValueChange(VetoableValueChangeEvent<float> valueChangeEvent) {
            if (valueChangeEvent.NewValue == 0f) {
                EventEmpty.Invoke();
            } else if (valueChangeEvent.NewValue == 1f) {
                EventFull.Invoke();
            }
        }

    }
}
