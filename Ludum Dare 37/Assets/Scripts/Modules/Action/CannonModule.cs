﻿using LD37.Util;
using UnityEngine;

namespace LD37.Modules.Action {

    public class CannonModule : FloatActionModule {

        [SerializeField]
        float repairPointsPerSecond = 1;

        [SerializeField]
        ParticleSystem darkSmoke;

        [SerializeField]
        ParticleSystem sparks;

        [SerializeField]
        ColliderShare collSharer;

        int startHealth;

        bool enteredRepairTrigger;

        float repairPointsPerUpdateFiller = 0;

        protected override void ValueChange(VetoableValueChangeEvent<float> changeEvent) {
            if (!Disabled) {
                var tmp = gameObject.transform.rotation.eulerAngles;
                tmp.z = changeEvent.NewValue * 180;
                gameObject.transform.rotation = Quaternion.Euler(tmp.x, tmp.y, tmp.z);
            } else {
                changeEvent.Veto();
            }
        }

        public CannonModule() {
            OnStart += StartHandler;
            OnUpdate += UpdateHandler;
            OnHealthChange += HealthChangeHandler;
        }

        private void StartHandler() {
            collSharer.EventTriggerEnter += (Collider2D coll) => TriggerEnter2D(coll);
            collSharer.EventTriggerExit += (Collider2D coll) => TriggerExit2D(coll);

            startHealth = Health;
            darkSmoke.Stop();
            sparks.Stop();
        }

        private void UpdateHandler() {
            if (enteredRepairTrigger) {
                if (UnityEngine.Input.GetKey(KeyCode.R)) {
                    repairPointsPerUpdateFiller += repairPointsPerSecond * Time.deltaTime;

                    if (repairPointsPerUpdateFiller > 1) {
                        if (Health < startHealth) {
                            if (Health + repairPointsPerUpdateFiller > startHealth) {
                                Health = startHealth;
                            } else {
                                ChangeHealth((int) repairPointsPerUpdateFiller);
                            }
                        }
                        repairPointsPerUpdateFiller = 0;
                    }
                }
            }
        }

        private void HealthChangeHandler(int previous, int current, int change) {
            if (current < 0) Health = 0;

            if (current != 0) {
                if (current < startHealth * 0.9) {
                    if (current < startHealth * 0.3)
                        sparks.Play();
                    darkSmoke.Play();
                    float healthPercentage = (float) current / startHealth;
                    darkSmoke.startColor = new Color(77 / 255f, 77 / 255f, 77 / 255f, 1 - healthPercentage);
                } else {
                    darkSmoke.Stop();
                    sparks.Stop();
                }
            }
        }

        private void TriggerEnter2D(Collider2D collision) {
            if (collision.gameObject.tag.Equals("Player")) {
                enteredRepairTrigger = true;
            }
        }

        private void TriggerExit2D(Collider2D collision) {
            if (collision.gameObject.tag.Equals("Player")) {
                enteredRepairTrigger = false;
            }
        }

        void OnDrawGizmos() {
            if (Health < startHealth && !Disabled) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(transform.position, 0.4f);
            } else if (Disabled) {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(transform.position, 0.4f);
            }
        }
    }
}
