﻿using UnityEngine;
using UnityEngine.Events;

namespace LD37.Modules.Action {
    public class CodeAction : BoolActionModule {

        public UnityEvent EventTrue {
            get {
                return eventTrue;
            }
        }

        public UnityEvent EventFalse {
            get {
                return eventFalse;
            }
        }

        [SerializeField]
        UnityEvent eventTrue;

        [SerializeField]
        UnityEvent eventFalse;

        protected override void ValueChange(VetoableValueChangeEvent<bool> changeEvent) {
            if (changeEvent.NewValue) {
                EventTrue.Invoke();
            } else {
                EventFalse.Invoke();
            }
        }
    }
}