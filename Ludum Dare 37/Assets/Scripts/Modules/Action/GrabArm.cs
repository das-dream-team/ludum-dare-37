﻿using UnityEngine;

namespace LD37.Modules.Action {
    [RequireComponent(typeof(Collider2D))]
    public class GrabArm : BoolActionModule {

        public float GrabDistance {
            get {
                return grabDistance;
            }

            set {
                grabDistance = value;
            }
        }

        public float SafeDistance {
            get {
                return safeDistance;
            }

            set {
                safeDistance = value;
            }
        }

        public Rigidbody2D GrabTarget { get { return grabTarget; } }

        [SerializeField]
        StatusLampHandler lamp;

        [SerializeField]
        float grabDistance = .3f;

        [SerializeField]
        float safeDistance = .015f;

        Rigidbody2D grabTarget;

        BoxCollider2D boxCollider;

        public GrabArm() {
            OnAwake += () => boxCollider = GetComponent<BoxCollider2D>();
        }

        protected override void ValueChange(VetoableValueChangeEvent<bool> changeEvent) {
            if (changeEvent.NewValue) {
                if (grabTarget == null) {
                    Vector2 lossyScale = transform.lossyScale;

                    Vector2 origin = (Vector2)transform.position + new Vector2(boxCollider.offset.x * lossyScale.x, boxCollider.offset.y * lossyScale.y);
                    Vector2 size = new Vector2(boxCollider.size.x * lossyScale.x, boxCollider.size.y * lossyScale.y);
                    float angle = transform.rotation.eulerAngles.z;
                    Vector2 direction = Vector2.down;
                    float distance = GrabDistance;

                    RaycastHit2D hit = Physics2D.BoxCast(origin, size, angle, direction, distance);
                    if (hit.rigidbody != null) {
                        hit.rigidbody.simulated = false;
                        hit.transform.parent = transform;
                        hit.transform.position = (Vector2)hit.transform.position
                            + (Vector2.up * hit.distance)
                            + (Vector2.down * SafeDistance);

                        grabTarget = hit.rigidbody;
                    } else {
                        lamp.SetLampRed();
                    }
                } else {
                    grabTarget.simulated = true;
                    grabTarget.transform.parent = null;

                    grabTarget = null;
                }
            }
        }

    }
}