﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD37.Modules.LocationSetter.Pressureplate {

    public class Plate : MonoBehaviour {
        Vector2 startingPosition;

        private void Start() {
            startingPosition = transform.localPosition;
        }

        // Update is called once per frame
        void Update() {
            transform.localPosition = new Vector2(startingPosition.x, transform.localPosition.y);

            if (transform.localPosition.y >= startingPosition.y) {
                transform.localPosition = new Vector2(transform.localPosition.x, startingPosition.y);
            }

            if (transform.localPosition.y <= 0) {
                transform.localPosition = new Vector2(transform.localPosition.x, 0);
            }

        }
    }
}
