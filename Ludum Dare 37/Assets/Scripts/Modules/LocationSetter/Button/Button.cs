﻿using UnityEngine;

namespace LD37.Modules.LocationSetter.Button {

    public class Button : MonoBehaviour {
        [SerializeField]
        GameObject buttonBase;

        Vector2 startingPosition;
        //0 = Left , 1 = Right
        [SerializeField]
        bool rotation;

        private void Start() {
            startingPosition = transform.localPosition;
        }
        // Update is called once per frame
        void Update() {
            if (rotation) {
                if (transform.localPosition.y <= startingPosition.y) {
                    transform.localPosition = new Vector2(startingPosition.x, startingPosition.y);
                }
            } else {
                if (transform.localPosition.y >= startingPosition.y) {
                    transform.localPosition = new Vector2(startingPosition.x, startingPosition.y);
                }
            }
        }
    }
}

