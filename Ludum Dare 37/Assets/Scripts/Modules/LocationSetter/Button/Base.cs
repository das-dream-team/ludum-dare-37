﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD37.Modules.LocationSetter.Button {

    public class Base : MonoBehaviour {

        Vector2 startingPosition;

        private void Start() {
            startingPosition = transform.localPosition;
        }

        // Update is called once per frame
        void Update() {
            transform.localPosition = new Vector2(startingPosition.x, startingPosition.y);
        }
    }
}
