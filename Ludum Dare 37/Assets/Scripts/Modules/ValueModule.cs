﻿using System.Collections.Generic;
using UnityEngine;

namespace LD37.Modules {
    public class ValueModule<T> : Module {

        public T Value {
            get {
                return value;
            }

            set {
                SetValue(Value, value, false);
            }
        }

        public event VetoableChangeHandler<T> OnChange;

        private T value;

        public void IntegrateChange(VetoableValueChangeEvent<T> valueChange) {
            if (!SetValue(valueChange.OldValue, valueChange.NewValue, valueChange.IsRevert)) {
                valueChange.Veto();
            }
        }

        public void VetoEvent(VetoableValueChangeEvent<T> vetoedEvent) {
            if (vetoedEvent.IsRevert) {
                Debug.LogWarning("Revert event cannot be vetoed, veto will be ignored");
            } else {
                SetValue(vetoedEvent.NewValue, vetoedEvent.OldValue, true);
            }
        }

        private bool SetValue(T oldValue, T newValue, bool isRevert) {
            // Return value is whether the change was successful 

            value = newValue;

            if (!EqualityComparer<T>.Default.Equals(oldValue, newValue) && OnChange != null) {
                VetoableValueChangeEvent<T> changeEvent = new VetoableValueChangeEvent<T>(this, oldValue, newValue, isRevert);
                OnChange(changeEvent);

                if (changeEvent.Vetoed && !changeEvent.IsRevert) {
                    VetoEvent(changeEvent);
                    return false;
                }
            }

            return true;
        }

    }
}
