﻿namespace LD37.Modules {
    public class ValueChangeEvent<T> {

        public ValueModule<T> Source {
            get; private set;
        }

        public T OldValue {
            get; private set;
        }

        public T NewValue {
            get; private set;
        }

        public ValueChangeEvent(ValueModule<T> source, T oldValue, T newValue) : this(oldValue, newValue) {
            Source = source;
        }

        private ValueChangeEvent(T oldValue, T newValue) {
            OldValue = oldValue;
            NewValue = newValue;
        }

    }

    public class VetoableValueChangeEvent<T> : ValueChangeEvent<T> {

        public bool Vetoed {
            get; private set;
        }

        public bool IsRevert {
            get; private set;
        }

        public VetoableValueChangeEvent(ValueModule<T> source, T oldValue, T newValue, bool isRevert) : base(source, oldValue, newValue) {
            IsRevert = isRevert;
        }

        public void Veto() {
            Vetoed = true;
        }

    }

    public delegate void ValueChangeHandler<T>(ValueChangeEvent<T> valueChangeEvent);
    public delegate void VetoableChangeHandler<T>(VetoableValueChangeEvent<T> valueChangeEvent);
}
