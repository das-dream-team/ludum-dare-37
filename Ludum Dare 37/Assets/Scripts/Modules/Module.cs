﻿using UnityEngine;

namespace LD37.Modules {
    public class Module : Entity {

        public bool Disabled {
            get {
                return disabled;
            }

            set {
                if (disabled != value) {
                    if (value) {
                        if (OnDisabled != null) {
                            OnDisabled();
                        }
                    } else {
                        if (OnEnabled != null) {
                            OnEnabled();
                        }
                    }
                }

                disabled = value;
            }
        }

        public bool AutoDisable {
            get {
                return autoDisable;
            }

            set {
                autoDisable = value;
            }
        }

        public bool AutoReenable {
            get {
                return autoReenable;
            }

            set {
                autoReenable = value;
            }
        }

        public int AutoDisableThreshold {
            get {
                return autoDisableThreshold;
            }

            set {
                autoDisableThreshold = value;
            }
        }

        public int AutoReenableThreshold {
            get {
                return autoReenableThreshold;
            }

            set {
                autoReenableThreshold = value;
            }
        }

        [SerializeField]
        bool disabled;

        [SerializeField]
        bool autoDisable;

        [SerializeField]
        bool autoReenable;

        [SerializeField]
        int autoDisableThreshold;

        [SerializeField]
        int autoReenableThreshold;

        public event System.Action OnDisabled;
        public event System.Action OnEnabled;

        public Module() {
            OnHealthChange += AutoDisableReenable;
        }

        private void AutoDisableReenable(int previous, int current, int change) {
            if (Disabled) {
                if (AutoReenable) {
                    if (current >= AutoReenableThreshold) {
                        Disabled = false;
                    }
                }
            } else {
                if (AutoDisable) {
                    if (current <= AutoDisableThreshold) {
                        Disabled = true;
                    }
                }
            }
        }

    }
}