﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusLampHandler : MonoBehaviour {

    public bool IsRed {
        get {
            return isRed;
        }

        set {
            isRed = value;
        }
    }

    [SerializeField]
    MeshRenderer lamp;

    [SerializeField]
    Light light;

    bool isRed;

    private void Start() {
        SetLampIdle();
    }

    public void SetLampIdle() {
        Renderer renderer = lamp.GetComponent<Renderer>();
        Material mat = lamp.GetComponent<Renderer>().materials[1];

        float emission = 1;
        Color baseColor = Color.yellow;

        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        mat.SetColor("_EmissionColor", finalColor);

        light.color = Color.yellow;
    }

    public void SetLampRed() {
        IsRed = true;
        StartCoroutine(WaitRed());
        Renderer renderer = lamp.GetComponent<Renderer>();
        Material mat = lamp.GetComponent<Renderer>().materials[1];

        float emission = 1;
        Color baseColor = Color.red;

        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        mat.SetColor("_EmissionColor", finalColor);

        light.color = Color.red;
    }

    public void SetLampOK() {
        Renderer renderer = lamp.GetComponent<Renderer>();
        Material mat = lamp.GetComponent<Renderer>().materials[1];

        float emission = 1;
        Color baseColor = Color.green;

        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        mat.SetColor("_EmissionColor", finalColor);

        light.color = Color.green;
    }

    IEnumerator WaitRed() {
        yield return new WaitForSeconds(1);
        IsRed = false;
    }
}
