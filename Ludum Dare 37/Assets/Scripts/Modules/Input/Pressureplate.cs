﻿using UnityEngine;

namespace LD37.Modules.Input {
    public class Pressureplate : BoolInputModule {

        [SerializeField]
        GameObject pressureplate;

        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.gameObject.Equals(pressureplate)) {
                    Value = true;
            }
        }
        private void OnTriggerExit2D(Collider2D collision) {
            if (collision.gameObject.Equals(pressureplate)) {
                    Value = false;
            }
        }

    }
}
