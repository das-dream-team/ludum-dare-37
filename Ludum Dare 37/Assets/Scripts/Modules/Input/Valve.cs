﻿using LD37.Util;
using UnityEngine;

namespace LD37.Modules.Input {
    public class Valve : FloatInputModule {

        public float SpeedMultiplier {
            get {
                return speedMultiplier;
            }

            set {
                speedMultiplier = value;
            }
        }

        public bool Invert {
            get {
                return invert;
            }

            set {
                invert = value;
            }
        }

        [SerializeField]
        float speedMultiplier = 60;

        [SerializeField]
        bool invert;

        bool playerInTrigger = false;

        public Valve() {
            OnChange += e => {
                float rotation = e.NewValue * 180;

                if (Invert) {
                    rotation = 180 - rotation;
                }

                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, rotation);
            };
        }

        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.gameObject.tag == "Player") {
                playerInTrigger = true;
            }

        }

        private void OnTriggerExit2D(Collider2D collision) {
            if (collision.gameObject.tag == "Player") {
                playerInTrigger = false;
            }
        }

        private void Update() {
            if (playerInTrigger) {
                bool q = UnityEngine.Input.GetKey(KeyCode.Q);
                bool e = UnityEngine.Input.GetKey(KeyCode.E);

                LineDirection? direction;
                if (q && !e) {
                    direction = LineDirection.Left;
                } else if (!q && e) {
                    direction = LineDirection.Right;
                } else {
                    direction = null;
                }

                if (direction != null) {
                    Vector3 rot = transform.eulerAngles;

                    float change = SpeedMultiplier * Time.deltaTime * (int)direction.Value;
                    rot.z = Mathf.Clamp(rot.z + change, 0, 180);

                    transform.eulerAngles = rot;

                    float value = Mathf.Abs(rot.z / 180);
                    if (invert) {
                        value = 1 - value;
                    }

                    Value = value;
                }
            }
        }

    }
}
