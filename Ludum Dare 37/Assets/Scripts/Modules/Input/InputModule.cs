﻿namespace LD37.Modules {
    public abstract class InputModule<T> : ValueModule<T> { }

    public abstract class BoolInputModule : InputModule<bool> { }

    public abstract class FloatInputModule : InputModule<float> { }

    public abstract class IntInputModule : InputModule<int> { }

}