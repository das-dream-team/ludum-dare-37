﻿using LD37.Modules;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD37.Modules.Input {
    public class Button : BoolInputModule {

        [SerializeField]
        GameObject button;

        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.gameObject.Equals(button)) {
                Value = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision) {
            if (collision.gameObject.Equals(button)) {
                Value = false;
            }
        }

    }
}
