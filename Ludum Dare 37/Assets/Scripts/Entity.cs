﻿using System;
using UnityEngine;

namespace LD37 {
    [Serializable]
    public class Entity : MonoBehaviour {

        public int Health {
            get {
                return health;
            }

            protected set {
                health = value;
            }
        }

        public bool Damageable {
            get {
                return damageable;
            }

            set {
                damageable = value;
            }
        }

        public bool Killable {
            get {
                return killable;
            }

            set {
                killable = value;
            }
        }

        [SerializeField]
        int health;

        [SerializeField]
        bool damageable;

        [SerializeField]
        bool killable;

        public delegate void HealthChangeHandler(int previous, int current, int change);

        public event Action OnAwake;
        public event Action OnStart;
        public event Action OnUpdate;
        public event Action OnLateUpdate;
        public event Action OnFixedUpdate;

        public event HealthChangeHandler OnHealthChange;
        public event HealthChangeHandler OnKilled;

        public void ChangeHealth(int amount) {
            if (Damageable) {
                int previous = Health;
                Health += amount;
                int current = Health;

                if (OnHealthChange != null) {
                    OnHealthChange(previous, current, amount);
                }

                if (Health <= 0) {
                    if (Killable) {
                        if (OnKilled != null) {
                            OnKilled(previous, current, amount);
                        }
                    }
                }

            }
        }

        private void Awake() {
            if (OnAwake != null) {
                OnAwake();
            }
        }

        private void Start() {
            if (OnStart != null) {
                OnStart();
            }
        }

        private void Update() {
            if (OnUpdate != null) {
                OnUpdate();
            }
        }

        private void LateUpdate() {
            if (OnLateUpdate != null) {
                OnLateUpdate();
            }
        }

        private void FixedUpdate() {
            if (OnFixedUpdate != null) {
                OnFixedUpdate();
            }
        }

    }
}
