﻿using UnityEngine;

namespace LD37 {
    public class Projectile : MonoBehaviour {

        public GameObject ExplosionEffect {
            get {
                return explosionEffect;
            }

            set {
                explosionEffect = value;
            }
        }

        public float ExplosionDuration {
            get {
                return explosionDuration;
            }

            set {
                explosionDuration = value;
            }
        }

        public int Damage {
            get {
                return damage;
            }

            set {
                damage = value;
            }
        }

        [SerializeField]
        GameObject explosionEffect;

        [SerializeField]
        float explosionDuration = 2;

        [SerializeField]
        int damage = 100;

        private void OnCollisionEnter2D(Collision2D collision) {
            Entity entity = collision.collider.GetComponent<Entity>();

            if (entity != null) {
                entity.ChangeHealth(-Damage);
            }

            if (ExplosionEffect) {
                GameObject obj = Instantiate(ExplosionEffect);
                obj.transform.position = transform.position;
                Destroy(obj, ExplosionDuration);
            }

            Destroy(gameObject);
        }

    }
}
