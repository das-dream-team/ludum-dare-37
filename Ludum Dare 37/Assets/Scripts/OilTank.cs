﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD37 {
    public class OilTank : MonoBehaviour {

        #region Fields

        [SerializeField]
        GameObject tankTop;

        [SerializeField]
        GameObject tankBottom;

        [SerializeField]
        float oilLevel = 1;
        #endregion

        #region Properties

        public float OilLevel {
            get {
                return oilLevel;
            }

            set {
                oilLevel = value;
            }
        }
        #endregion

        float lastValue;
        float totalValue;

        // Use this for initialization
        void Start() {
            totalValue = tankTop.transform.localPosition.y - tankBottom.transform.localPosition.y;
            lastValue = 1;
        }

        // Update is called once per frame
        void Update() {
            if(OilLevel != lastValue) {
                    tankTop.transform.localPosition = new Vector2(tankTop.transform.localPosition.x, totalValue * OilLevel);

                    lastValue = OilLevel;
            }
        }
    }
}
