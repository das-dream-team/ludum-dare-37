﻿using System;
using System.Collections;
using UnityEngine;

namespace LD37{
    public class ConveyorProcessing : MonoBehaviour {

        [SerializeField]
        OilTank oilTank;

        [SerializeField]
        ParticleSystem poof;

        private void OnTriggerEnter2D(Collider2D collision) {
            Item item = collision.gameObject.gameObject.GetComponent<Item>();
            if(item != null) {
                if(oilTank.OilLevel + item.ProcessedOilValue / 100 <= 1) {
                    oilTank.OilLevel += item.ProcessedOilValue / 100;
                }
                
                StartCoroutine(WaitForDestroy(collision));
                poof.Play();
            }
        }

        IEnumerator WaitForDestroy(Collider2D collision) {
            yield return new WaitForSeconds(0.5f);
            Destroy(collision.gameObject);
        }
    }
}

