﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LD37 {
    [RequireComponent(typeof(Slider))]
    public class HealthBar : MonoBehaviour {
        [SerializeField]
        Entity health;

        [SerializeField]
        Image[] images;

        Color[] startColors;

        Slider slider;

        float h;
        float s;
        float v;

        void Start() {
            slider = GetComponent<Slider>();
            slider.maxValue = health.Health;
            startColors = new Color[images.Length];
            for (int i = 0; i < images.Length; i++) {
                startColors[i] = images[i].color;
            }
        }

        // Update is called once per frame
        void Update() {
            slider.value = health.Health;
            for (int i = 0; i < images.Length; i++) {
                if (slider.value >= slider.maxValue) {
                    images[i].color = new Color(startColors[i].r, startColors[i].g, startColors[i].b,0);
                } else {
                    images[i].color = startColors[i];
                }
                    
            }
        }
    }
}
