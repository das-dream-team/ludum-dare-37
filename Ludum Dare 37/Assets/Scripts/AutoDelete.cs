﻿using UnityEngine;

namespace LD37 {
    public class AutoDelete : MonoBehaviour {

        public float DestructionTimer { get; set; }

        public float CreationRealtime { get; private set; }

        void Awake() {
            CreationRealtime = Time.realtimeSinceStartup;
        }

        private void Update() {
            if (Time.realtimeSinceStartup - CreationRealtime >= DestructionTimer) {
                Destroy(gameObject);
            }
        }

    }
}
