﻿using UnityEngine;

namespace LD37.Characters {
    [RequireComponent(typeof(MovementMotor))]
    public class Character : Entity {

        public MovementMotor MovementMotor {
            get; private set;
        }

        public Character() {
            OnAwake += () =>  MovementMotor = GetComponent<MovementMotor>();
        }

    }
}