﻿using LD37.Util;
using System.Collections;
using UnityEngine;

namespace LD37.Characters {
    [RequireComponent(typeof(BallisticMotor))]
    public class BallisticAI : ArtificialIntelligence {

        #region Properties
        public GameObject Target {
            get {
                return target;
            }
            set {
                target = value;
            }
        }

        public float MinDistanceToTarget {
            get {
                return minDistanceToTarget;
            }
            set {
                minDistanceToTarget = value;
            }
        }

        public float ThrowDelay {
            get {
                return Random.Range(throwDelayMin, throwDelayMax);
            }
        }

        public float ThrowHeight {
            get {
                return throwHeight;
            }

            set {
                throwHeight = value;
            }
        }

        public float FireRange {
            get {
                return fireRange;
            }

            set {
                fireRange = value;
            }
        }

        public float RetreatInitiate {
            get {
                return retreatInitiate;
            }

            set {
                retreatInitiate = value;
            }
        }

        public float RetreatSafety {
            get {
                return retreatSafety;
            }

            set {
                retreatSafety = value;
            }
        }

        public float DetectionDistance {
            get {
                return detectionDistance;
            }

            set {
                detectionDistance = value;
            }
        }

        public bool TargetEntireHouse {
            get {
                return targetEntireHouse;
            }

            set {
                targetEntireHouse = value;
            }
        }

        public bool Retreating { get; private set; }

        public float DeathDuration {
            get {
                return deathDuration;
            }

            set {
                deathDuration = value;
            }
        }
        [SerializeField]
        private GameObject bloodParticle;

        public GameObject DisappearEffect {
            get {
                return disappearEffect;
            }

            set {
                disappearEffect = value;
            }
        }

        public float DisappearDuration {
            get {
                return disappearDuration;
            }

            set {
                disappearDuration = value;
            }
        }
        #endregion

        #region Serialized Fields
        [SerializeField]
        GameObject target;

        [SerializeField]
        float minDistanceToTarget;

        [SerializeField]
        float throwDelayMin = 1;

        [SerializeField]
        float throwDelayMax = 4;

        [SerializeField]
        float throwHeight = 1;

        [SerializeField]
        float fireRange = 10;

        [SerializeField]
        float retreatInitiate = 2;

        [SerializeField]
        float retreatSafety = 3.5f;

        [SerializeField]
        float detectionDistance = 15;

        [SerializeField]
        bool targetEntireHouse = true;

        [SerializeField]
        GameObject bloodEfects;

        [SerializeField]
        Transform bloodOrigin;

        [SerializeField]
        float deathDuration = 2;

        [SerializeField]
        GameObject disappearEffect;

        [SerializeField]
        float disappearDuration = 2;
        #endregion

        bool dead;

        BallisticMotor ballisticMotor;

        public BallisticAI() {
            OnAwake += AwakeHandler;
            OnStart += StartHandler;
            OnFixedUpdate += FixedUpdateHandler;
            OnKilled += KilledHandler;
        }

        private void KilledHandler(int previous, int current, int change) {
            dead = true;
            StartCoroutine(CDie());
        }

        private IEnumerator CDie() {
            MovementMotor.Animator.SetTrigger("die");

            bloodParticle.SetActive(true);
            GetComponent<Rigidbody2D>().simulated = false;

            yield return new WaitForSeconds(DeathDuration);

            if (DisappearEffect != null) {
                GameObject obj = Instantiate(DisappearEffect);
                obj.transform.position = transform.position;

                Destroy(obj, DisappearDuration);
            }

            Destroy(gameObject);
        }

        void AwakeHandler() {
            ballisticMotor = GetComponent<BallisticMotor>();
        }

        void StartHandler() {
            if (TargetEntireHouse) {
                Target = GameObject.FindGameObjectWithTag("House");
            }

            StartCoroutine(CStartThrowing());
        }

        void FixedUpdateHandler() {
            if (Target != null) {
                float offset = Target.transform.position.x - transform.position.x;
                float distance = Mathf.Abs(offset);
                LineDirection direction = (LineDirection)(int)Mathf.Sign(offset);
                
                if (Retreating) {
                    MovementMotor.Animator.SetInteger("direction", -(int)direction);

                    if (distance >= RetreatSafety) {
                        Retreating = false;
                    } else {
                        MovementMotor.Move((LineDirection)(-(int)direction));
                    }
                } else if (/*distance <= DetectionDistance*/true) {
                    MovementMotor.Animator.SetInteger("direction", (int)direction);

                    if (distance >= FireRange) {
                        MovementMotor.Move(direction);
                    } else if (distance <= RetreatInitiate) {
                        Retreating = true;
                        MovementMotor.Move((LineDirection)(-(int)direction));
                    }
                }
            }
        }

        IEnumerator CStartThrowing() {
            while (true) {
                if (dead) {
                    yield break;
                }

                if (!Retreating) {
                    float offset = Target.transform.position.x - transform.position.x;
                    float distance = Mathf.Abs(offset);
                    LineDirection direction = (LineDirection)(int)Mathf.Sign(offset);

                    if (Target != null) {
                        if (distance <= FireRange && distance >= RetreatInitiate) {
                            ballisticMotor.ThrowAt(Target.transform, ThrowHeight, () => Retreating);
                        }
                    }
                }

                yield return new WaitForSeconds(ThrowDelay);
            }
        }

    }
}