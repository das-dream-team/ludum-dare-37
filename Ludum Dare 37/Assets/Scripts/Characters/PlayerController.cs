﻿using LD37.Util;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LD37.Characters {
    public class PlayerController : Character {

        [SerializeField]
        Entity house;

        public PlayerController() {
            OnFixedUpdate += FixedUpdateHandler;
        }

        private void FixedUpdateHandler() {
            if (house == null || house.Health > 0) {
                bool left = Input.GetKey(KeyCode.A);
                bool right = Input.GetKey(KeyCode.D);
                bool up = Input.GetKey(KeyCode.W);
                bool down = Input.GetKey(KeyCode.S);
                bool grabClimbable = Input.GetKeyDown(KeyCode.Space);

                if (grabClimbable) {
                    if (!MovementMotor.IsGrabbingClimbable) {
                        MovementMotor.GrabClimbable();
                    } else {
                        MovementMotor.ReleaseClimbable();
                    }
                }

                if (left && !right) {
                    MovementMotor.Move(LineDirection.Left);
                } else if (!left && right) {
                    MovementMotor.Move(LineDirection.Right);
                }

                if (MovementMotor.IsGrabbingClimbable) {
                    if (up) {
                        MovementMotor.Climb(LineDirection.Up);
                    } else if (down) {
                        MovementMotor.Climb(LineDirection.Down);
                    }
                } else {
                    if (up) {
                        MovementMotor.Jump();
                    }
                }
            } else {
                if (Input.GetKey(KeyCode.Space)) {
                    SceneManager.LoadScene("Menu");
                    house = null;
                }
            }
        }
    }
}