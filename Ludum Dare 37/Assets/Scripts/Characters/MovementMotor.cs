﻿using LD37.Util;
using System.Collections;
using UnityEngine;

namespace LD37.Characters {
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class MovementMotor : MonoBehaviour {

        #region Properties
        public Collider2D Collider {
            get {
                return GetComponent<Collider2D>();
            }
        }

        public Rigidbody2D Rigidbody { get; private set; }

        public bool IsGrabbingClimbable { get; private set; }

        public bool IsStandingOnGround { get; private set; }

        public Animator Animator {
            get {
                return animator;
            }
            set {
                animator = value;
            }
        }

        public float Acceleration {
            get {
                return walkingAcceleration;
            }
            set {
                walkingAcceleration = value;
            }
        }

        public float MaxWalkingVelocity {
            get {
                return maxWalkingVelocity;
            }
            set {
                maxWalkingVelocity = value;
            }
        }

        public float JumpForce {
            get {
                return jumpForce;
            }
            set {
                jumpForce = value;
            }
        }

        public float Deceleration {
            get {
                return walkingDeceleration;
            }
            set {
                walkingDeceleration = value;
            }
        }

        public float RaycastLength {
            get {
                return raycastLength;
            }
            set {
                raycastLength = value;
            }
        }

        public float MaxClimbingVelocity {
            get {
                return maxClimbingVelocity;
            }

            set {
                maxClimbingVelocity = value;
            }
        }

        public float ClimbingAcceleration {
            get {
                return climbingAcceleration;
            }

            set {
                climbingAcceleration = value;
            }
        }

        public float Mass {
            get {
                return Rigidbody.mass;
            }
        }
        #endregion

        #region Serialized Fields

        [SerializeField]
        private Animator animator;

        [SerializeField][Header("Only needed if inside the house")]
        private Rigidbody2D house;

        [SerializeField]
        private float walkingAcceleration = 12;

        [SerializeField]
        private float walkingDeceleration = 7;

        [SerializeField]
        private float maxWalkingVelocity = 10;

        [SerializeField]
        private float climbingAcceleration = 7;

        [SerializeField]
        private float climbingDeceleration = 14;

        [SerializeField]
        private float maxClimbingVelocity = 2;

        [SerializeField]
        private float jumpForce = 120;

        [SerializeField]
        private float raycastLength = 0.5f;

        #endregion

        bool walkedInUpdate;
        bool climbedInUpdate;

        LineDirection lastDirection;

        Collider2D climbable;

        private void Awake() {
            if (Animator == null) {
                Animator = GetComponent<Animator>();
            }
        }

        private void Start() {
            Rigidbody = GetComponent<Rigidbody2D>();

            walkedInUpdate = false;
            IsGrabbingClimbable = false;
            IsStandingOnGround = false;
            climbable = null;

            StartCoroutine(LateFixedUpdate());
        }

        private void FixedUpdate() {
            if (house != null) {
                Rigidbody.velocity = new Vector2(house.velocity.x, Rigidbody.velocity.y);
            }

            if (IsGrabbingClimbable) {
                float percentage = Mathf.Clamp01(Rigidbody.velocity.y / MaxClimbingVelocity);

                if (Animator != null) {
                    Animator.SetFloat("climbSpeed", percentage);
                }
            }

            if (!walkedInUpdate) {
                if (Mathf.Abs(house != null ? Rigidbody.velocity.x - house.velocity.x : Rigidbody.velocity.x) > 0) {
                    Rigidbody.AddForce(new Vector2(Rigidbody.velocity.x * walkingDeceleration * -1, 0) * Mass);
                }

                if (Animator != null) {
                    Animator.SetBool("isMoving", false);
                }
            }

            if (!climbedInUpdate) {
                if (IsGrabbingClimbable) {
                    if (Mathf.Abs(Rigidbody.velocity.y) > 0) {
                        Rigidbody.AddForce(new Vector2(0, Rigidbody.velocity.y * climbingDeceleration * -1));
                    }
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision) {
            RaycastHit2D rayHit = Physics2D.BoxCast(transform.position, new Vector2(Collider.bounds.size.x, RaycastLength), 0, Vector2.down, RaycastLength);

            if (rayHit.collider != null) {
                IsStandingOnGround = true;
            }
        }

        private void OnCollisionExit2D(Collision2D collision) {
            RaycastHit2D rayHit = Physics2D.BoxCast(transform.position, new Vector2(Collider.bounds.size.x, RaycastLength), 0, Vector2.down, RaycastLength);

            if (rayHit.collider == null) {
                IsStandingOnGround = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.tag == "Climbable") {
                climbable = collision;
            }
        }

        private void OnTriggerExit2D(Collider2D collision) {
            if (collision == climbable) {
                climbable = null;
                ReleaseClimbable();
            }
        }

        private IEnumerator LateFixedUpdate() {
            while (true) {
                yield return new WaitForFixedUpdate();
                walkedInUpdate = false;
                climbedInUpdate = false;
            }
        }

        public void Move(LineDirection direction) {
            if (!IsGrabbingClimbable) {
                if (Animator != null) {
                    Animator.SetInteger("direction", (int)direction);
                    Animator.SetBool("isMoving", true);
                }
            } else {
                if (Animator != null) {
                    Animator.SetInteger("direction", 2);
                }
            }

            if (Mathf.Abs(Rigidbody.velocity.x) < maxWalkingVelocity) {
                float directedAcceleration = (int)direction * walkingAcceleration * 10;
                Rigidbody.AddForce(new Vector2(directedAcceleration, 0) * Mass);
            }

            walkedInUpdate = true;
        }

        public void Jump() {
            if (IsStandingOnGround) {
                IsStandingOnGround = false;
                Rigidbody.AddForce(Vector2.up * jumpForce * Mass);
            }
        }

        public void GrabClimbable() {
            if (climbable != null) {
                if (Animator != null) {
                    Animator.SetBool("isClimbing", true);
                    Animator.SetInteger("direction", 2);
                }

                Rigidbody.velocity = Vector2.zero;
                Rigidbody.gravityScale = 0;
                IsGrabbingClimbable = true;
            }
        }

        public void ReleaseClimbable() {
            if (Animator != null) {
                Animator.SetBool("isClimbing", false);
            }

            Rigidbody.gravityScale = 1;
            IsGrabbingClimbable = false;
        }

        public void Climb(LineDirection direction) {
            if (IsGrabbingClimbable) {
                float directedAcceleration = (int)direction * ClimbingAcceleration;

                if (Mathf.Abs(Rigidbody.velocity.y) < MaxClimbingVelocity) {
                    Rigidbody.AddForce(new Vector2(0, directedAcceleration));
                }
            }

            climbedInUpdate = true;
        }
    }
}