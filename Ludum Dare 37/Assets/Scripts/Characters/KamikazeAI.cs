﻿using LD37.Util;
using UnityEngine;

namespace LD37.Characters {
    public class KamikazeAI : ArtificialIntelligence {

        #region Properties
        public GameObject Target {
            get {
                return target;
            }

            set {
                target = value;
            }
        }

        public float DetectionDistance {
            get {
                return detectionDistance;
            }

            set {
                detectionDistance = value;
            }
        }

        public bool AutoTargetHouse {
            get {
                return autoTargetHouse;
            }

            set {
                autoTargetHouse = value;
            }
        }
        #endregion

        #region Serialized Fields
        [SerializeField]
        GameObject target;

        [SerializeField]
        float detectionDistance = 10;

        [SerializeField]
        bool autoTargetHouse = true;

        [SerializeField]
        GameObject explosionEffect;

        [SerializeField]
        float explosionDuration = 2;

        [SerializeField]
        float explosionRange = 4;

        [SerializeField]
        int damage;

        [SerializeField]
        float explosionRadius = 2;
        #endregion

        public KamikazeAI() {
            OnStart += StartHandler;
            OnFixedUpdate += FixedUpdateHandler;
            OnKilled += KilledHandler;
        }

        void StartHandler() {
            if (AutoTargetHouse) {
                Target = GameObject.FindGameObjectWithTag("House");
            }
        }

        void FixedUpdateHandler() {
            if (Target != null) {
                float offset = Target.transform.position.x - transform.position.x;
                float distance = Mathf.Abs(offset);
                LineDirection direction = (LineDirection)(int)Mathf.Sign(offset);

                MovementMotor.Animator.SetInteger("direction", (int)direction);

                if (distance <= explosionRange) {
                    Explode();
                } else {
                    MovementMotor.Move(direction);
                }
            }
        }

        void KilledHandler(int previous, int current, int change) {
            if (explosionEffect != null) {
                GameObject obj = Instantiate(explosionEffect);
                obj.transform.position = transform.position;
                Destroy(obj, explosionDuration);
            }

            Destroy(gameObject);
        }

        public void Explode() {
            ChangeHealth(-Health);
            RaycastHit2D[] rayHits = Physics2D.CircleCastAll(transform.position, explosionRadius, Vector2.zero);
            
            foreach (RaycastHit2D rayHit in rayHits) {
                Entity e = rayHit.collider.GetComponentInParent<Entity>();

                if (e != null) {
                    e.ChangeHealth(-damage);
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision) {
            if (collision.gameObject.tag == "House") {
                Explode();
            }
        }

    }
}