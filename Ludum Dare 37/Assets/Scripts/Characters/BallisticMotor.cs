﻿using LD37.Util;
using System;
using System.Collections;
using UnityEngine;

namespace LD37.Characters {
    public class BallisticMotor : MonoBehaviour {

        public GameObject ProjectilePrefab {
            get {
                return projectilePrefab;
            }
            set {
                projectilePrefab = value;
            }
        }

        public Animator Animator {
            get {
                return animator;
            }

            set {
                animator = value;
            }
        }

        public Vector2 Offset {
            get {
                return offset;
            }

            set {
                offset = value;
            }
        }

        public float ProjectileVelocity {
            get {
                return projectileVelocity;
            }

            set {
                projectileVelocity = value;
            }
        }

        public float ShootDelay {
            get {
                return shootDelay;
            }

            set {
                shootDelay = value;
            }
        }

        [SerializeField]
        Animator animator;

        [SerializeField]
        GameObject projectilePrefab;

        [SerializeField]
        Vector2 offset;

        [SerializeField]
        float projectileVelocity;

        [SerializeField]
        float shootDelay;

        private void Awake() {
            if (Animator == null) {
                Animator = GetComponent<Animator>();
            }
        }

        public void Throw(Vector2 velocity, Vector2 spawnOffset) {
            if (ProjectilePrefab == null) {
                Debug.LogError("Projectile is null!");
                return;
            }

            GameObject projectile = Instantiate(ProjectilePrefab, transform);
            projectile.SetActive(true);
            projectile.transform.parent = null;
            projectile.transform.position = transform.position + (Vector3)spawnOffset;
            projectile.GetComponent<Rigidbody2D>().velocity = velocity;
        }

        public void ThrowAt(Vector2 target, float height) {
            ThrowAt(target, height, null);
        }

        public void ThrowAt(Transform transform, float height) {
            ThrowAt(transform, height, null);
        }

        public void ThrowAt(Vector2 target, float height, Func<bool> cancel) {
            if (Animator != null) {
                Animator.SetTrigger("throwProjectile");
            }

            StartCoroutine(CThrow(target, height, cancel));
        }

        public void ThrowAt(Transform transform, float height, Func<bool> cancel) {
            ThrowAt(transform.position, height, cancel);
        }

        private Vector2 CalculateLaunchVelocity(Vector2 target, Vector2 start, float height) {
            // To understand the algorithm, watch this video: https://youtu.be/IvT8hjy6q4o
            float gravity = Physics2D.gravity.y;

            float displacementY = target.y - start.y;
            float displacementX = target.x - start.x;

            float velocityY = Mathf.Sqrt(-2 * gravity * height);
            float velocityX = displacementX / Mathf.Sqrt(-2 * height / gravity) * Mathf.Sqrt(2 * (displacementY - height) / gravity);

            return new Vector2(velocityX, velocityY * -Mathf.Sign(gravity));
        }

        private IEnumerator CThrow(Vector2 target, float height, Func<bool> cancel) {
            yield return new WaitForSeconds(ShootDelay);

            if (cancel != null && cancel()) {
                yield break;
            }

            LineDirection direction = (LineDirection)(int)Mathf.Sign((target - (Vector2)transform.position).x);
            Vector2 calcOffset = new Vector2(offset.x * (int)direction, offset.y);

            Throw(CalculateLaunchVelocity(target, (Vector2)transform.position + calcOffset, height), calcOffset);
        }

    }
}