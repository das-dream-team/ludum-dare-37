﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD37 {
    public class CameraController : MonoBehaviour {

        private Vector3 targetPosition;

        #region Serialized fields

        [SerializeField]
        private float nearZoomLimit;

        [SerializeField]
        private float farZoomLimit;

        [SerializeField]
        private float focusChange;

        [SerializeField]
        private Transform farFocus;

        [SerializeField]
        private Vector2 farFocusOffset;

        [SerializeField]
        private Transform nearFocus;

        [SerializeField]
        private Vector2 nearFocusOffset;

        #endregion

        // TODO: Add properties for all serialized fields
        #region Properties

        public Transform CurrentFocus { get; set; }

        public Vector2 CurrentFocusOffset { set; get; }

        public float NearZoomLimit {
            get {
                return nearZoomLimit;
            }

            set {
                nearZoomLimit = value;
            }
        }

        public float FarZoomLimit {
            get {
                return farZoomLimit;
            }

            set {
                farZoomLimit = value;
            }
        }

        public float FocusChange {
            get {
                return focusChange;
            }

            set {
                focusChange = value;
            }
        }

        public Transform FarFocus {
            get {
                return farFocus;
            }

            set {
                farFocus = value;
            }
        }

        public Vector2 FarFocusOffset {
            get {
                return farFocusOffset;
            }

            set {
                farFocusOffset = value;
            }
        }

        public Transform NearFocus {
            get {
                return nearFocus;
            }

            set {
                nearFocus = value;
            }
        }

        public Vector2 NearFocusOffset {
            get {
                return nearFocusOffset;
            }

            set {
                nearFocusOffset = value;
            }
        }

        #endregion

        private void Start() {
            CurrentFocus = transform.position.z > FocusChange ? NearFocus : FarFocus;
            CurrentFocusOffset = transform.position.z > FocusChange ? NearFocusOffset : FarFocusOffset;
            targetPosition = new Vector3(CurrentFocus.position.x + CurrentFocusOffset.x, CurrentFocus.position.y + CurrentFocusOffset.y, transform.position.z);
        }

        private void Update() {
            float input = Input.GetAxis("Mouse ScrollWheel");
            if (input > 0 && targetPosition.z + 1 <= NearZoomLimit) {
                targetPosition.z += 1;
            } else if (input < 0 && targetPosition.z - 1 >= FarZoomLimit) {
                targetPosition.z -= 1;
            }

            if (transform.position != targetPosition) {
                Vector3 velocity = Vector3.zero;
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, 0.1f);
            }

            if (transform.position.z > FocusChange) {
                targetPosition.x = NearFocus.position.x + NearFocusOffset.x;
                targetPosition.y = NearFocus.position.y + NearFocusOffset.y;
            } else if (transform.position.z -1 < FocusChange) {
                targetPosition.x = FarFocus.position.x + FarFocusOffset.x;
                targetPosition.y = FarFocus.position.y + FarFocusOffset.y;
            }
        }
    }
}
