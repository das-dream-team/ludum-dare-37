﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace LD37 {
    public class MainMenuActions : MonoBehaviour {

        [SerializeField]
        GameObject pressurePlate;

        public void ActionTutorial() {
            SceneManager.LoadScene("Tutorial");
        }

        public void ActionStart() {
            SceneManager.LoadScene("Level");
        }

        public void ActionExit() {
            pressurePlate.SetActive(true);
        }

        public void ActionActualExit() {
            Application.Quit();
        }

    }
}
