﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWarning : MonoBehaviour {
    [SerializeField]
    Animator img;

    List<GameObject> enemiesInside;
    


    void OnTriggerStay2D(Collider2D other) {
        if(other.tag == "Enemy") {
            img.SetBool("enemyIsNearby", true);
            StartCoroutine(resetWarning());
        }
    }

    IEnumerator resetWarning() {
        yield return new WaitForSeconds(1f);
        img.SetBool("enemyIsNearby", false);
    }
}
