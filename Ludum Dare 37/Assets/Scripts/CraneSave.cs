﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LD37 {
    public class CraneSave : MonoBehaviour {
        [SerializeField]
        Vector3 resetPostion;

        // Update is called once per frame
        void Update() {
            if(transform.rotation.eulerAngles.z > 270 || transform.rotation.eulerAngles.z < 80) {
                transform.rotation = Quaternion.Euler(resetPostion);
            }
        }
    }
}
