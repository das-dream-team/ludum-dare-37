﻿using System.Collections.Generic;
using UnityEngine;

namespace LD37 {
    class HealthRelay : Entity {

        public List<Entity> Destinations {
            get {
                return destinations;
            }
        }

        [SerializeField]
        private List<Entity> destinations;

        public HealthRelay() {
            OnHealthChange += (prev, curr, delta) => {
                Destinations.ForEach(e => {
                    e.ChangeHealth(Health);
                });
            };
        }

    }
}
