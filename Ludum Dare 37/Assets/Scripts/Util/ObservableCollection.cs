﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LD37.Util {
    [Serializable]
    public class ObservableCollection<T> : ICollection<T> {

        public delegate void CollectionChangeHandler(ICollection<T> items);

        public event CollectionChangeHandler OnInsertion;
        public event CollectionChangeHandler OnTruncation;

        readonly ICollection<T> collection;

        public ObservableCollection(ICollection<T> collection) {
            if (collection == null) {
                throw new ArgumentNullException("ICollection to wrap may not be a null-reference");
            }

            this.collection = collection;
        }

        public int Count {
            get {
                return collection.Count;
            }
        }

        public bool IsReadOnly {
            get {
                return collection.IsReadOnly;
            }
        }

        public void Add(T item) {
            collection.Add(item);

            if (OnInsertion != null) {
                OnInsertion(new List<T>() { item });
            }
        }

        public void Clear() {
            ICollection<T> copy = new List<T>();
            
            foreach (T item in collection) {
                copy.Add(item);
            }

            collection.Clear();

            if (OnTruncation != null) {
                OnTruncation(copy);
            }
        }

        public bool Contains(T item) {
            return collection.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex) {
            collection.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator() {
            return collection.GetEnumerator();
        }

        public bool Remove(T item) {
            if (collection.Remove(item)) {
                if (OnTruncation != null) {
                    OnTruncation(new List<T>() { item });
                }

                return true;
            }

            return false;
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return collection.GetEnumerator();
        }
    }
}