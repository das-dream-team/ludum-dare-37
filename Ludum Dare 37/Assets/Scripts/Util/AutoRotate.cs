﻿using UnityEngine;

namespace LD37.Util {
    public class AutoRotate : MonoBehaviour {

        [SerializeField]
        Vector3 rotationPerSecond;

        private void Update() {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + (rotationPerSecond * Time.deltaTime));
        }

    }
}
