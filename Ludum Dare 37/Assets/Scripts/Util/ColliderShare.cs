﻿using System;
using UnityEngine;

namespace LD37.Util {
    public class ColliderShare : MonoBehaviour {

        public event Action<Collider2D> EventTriggerEnter, EventTriggerStay, EventTriggerExit;
        public event Action<Collision2D> EventCollisionEnter, EventCollisionStay, EventCollisionExit;

        private void OnTriggerEnter2D(Collider2D collider) {
            if (EventTriggerEnter != null) {
                EventTriggerEnter(collider);
            }
        }

        private void OnTriggerStay2D(Collider2D collider) {
            if (EventTriggerStay != null) {
                EventTriggerStay(collider);
            }
        }

        private void OnTriggerExit2D(Collider2D collider) {
            if (EventTriggerExit != null) {
                EventTriggerExit(collider);
            }
        }

        private void OnCollisionEnter2D(Collision2D collision) {
            if (EventCollisionEnter != null) {
                EventCollisionEnter(collision);
            }
        }

        private void OnCollisionStay2D(Collision2D collision) {
            if (EventCollisionStay != null) {
                EventCollisionStay(collision);
            }
        }

        private void OnCollisionExit2D(Collision2D collision) {
            if (EventCollisionExit != null) {
                EventCollisionExit(collision);
            }
        }

    }
}
