﻿using UnityEngine;

namespace LD37.Utils {
    public class RotateGears : MonoBehaviour {

        [SerializeField]
        float value;

        // Update is called once per frame
        void Update() {
            transform.rotation.SetEulerRotation(transform.rotation.y + value, 90, 90);
        }
    }
}
