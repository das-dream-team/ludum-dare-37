﻿namespace LD37.Util {
    public enum PlaneDirection {
        Left = -1, West = Left,
        Right = 1, East = Right,
        Up = 2, Top = Up, North = Up,
        Down = 3, Bottom = Down, South = Down
    }

    public enum LineDirection {
        Left = -1, Down = -1,
        Right = 1, Up = 1
    }
}