﻿using UnityEngine;

public class SimpleCameraController : MonoBehaviour {

    [SerializeField]
    Transform target;

    [SerializeField]
    Vector2 offset;

    [SerializeField]
    float zDistance;

    public Transform Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    private void Update() {
        transform.position = new Vector3(Target.position.x + offset.x, Target.position.y + offset.y, Target.position.z + zDistance);
    }
}
