﻿using LD37.Modules;
using LD37.Modules.Action;
using LD37.Util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace LD37 {
    [RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
    public class House : Entity {

        private Collider2D[] colliders;

        #region Properties

        public Rigidbody2D Rigidbody { get; private set; }

        public Animator Animator { get; private set; }

        public float Velocity {
            get {
                return velocity;
            }

            set {
                velocity = value;
            }
        }

        public BoolInputModule InputWalkLeft {
            get {
                return inputWalkLeft;
            }

            set {
                inputWalkLeft = value;
            }
        }

        public BoolInputModule InputWalkRight {
            get {
                return inputWalkRight;
            }

            set {
                inputWalkRight = value;
            }
        }

        public FloatInputModule InputCrane {
            get {
                return inputCrane;
            }

            set {
                inputCrane = value;
            }
        }

        public BoolInputModule InputGrab {
            get {
                return inputGrab;
            }

            set {
                inputGrab = value;
            }
        }

        public BoolInputModule InputConveyor {
            get {
                return inputConveyor;
            }

            set {
                inputConveyor = value;
            }
        }

        public BoolInputModule InputWeaponShootRight {
            get {
                return inputWeaponShootRight;
            }

            set {
                inputWeaponShootRight = value;
            }
        }

        public BoolInputModule InputWeaponShootLeft {
            get {
                return inputWeaponShootLeft;
            }

            set {
                inputWeaponShootLeft = value;
            }
        }

        public FloatInputModule InputWeaponMoveLeft {
            get {
                return inputWeaponMoveLeft;
            }

            set {
                inputWeaponMoveLeft = value;
            }
        }

        public FloatInputModule InputWeaponMoveRight {
            get {
                return inputWeaponMoveRight;
            }

            set {
                inputWeaponMoveRight = value;
            }
        }

        public List<FloatActionModule> ActionCrane {
            get {
                return actionCrane;
            }
        }

        public List<BoolActionModule> ActionGrab {
            get {
                return actionGrab;
            }

        }

        public List<BoolActionModule> ActionConveyor {
            get {
                return actionConveyor;
            }

        }

        public List<BoolActionModule> ActionWeaponShootLeft {
            get {
                return actionWeaponShootLeft;
            }

        }

        public List<BoolActionModule> ActionWeaponShootRight {
            get {
                return actionWeaponShootRight;
            }
        }

        public List<FloatActionModule> ActionWeaponMoveLeft {
            get {
                return actionWeaponMoveLeft;
            }
        }

        public List<FloatActionModule> ActionWeaponMoveRight {
            get {
                return actionWeaponMoveRight;
            }
        }

        #endregion

        #region Serialized Fields

        [SerializeField]
        GameObject gameOverUI;

        [SerializeField]
        GameObject gameOverExplosion;

        [SerializeField]
        float velocity = 1;

        [SerializeField]
        BoolInputModule inputWalkLeft;

        [SerializeField]
        BoolInputModule inputWalkRight;

        [SerializeField]
        FloatInputModule inputCrane;

        [SerializeField]
        BoolInputModule inputGrab;

        [SerializeField]
        BoolInputModule inputConveyor;

        [SerializeField]
        BoolInputModule inputWeaponShootRight;

        [SerializeField]
        BoolInputModule inputWeaponShootLeft;

        [SerializeField]
        FloatInputModule inputWeaponMoveRight;

        [SerializeField]
        FloatInputModule inputWeaponMoveLeft;

        [SerializeField]
        List<FloatActionModule> actionCrane;

        [SerializeField]
        List<BoolActionModule> actionGrab;

        [SerializeField]
        List<BoolActionModule> actionConveyor;

        [SerializeField]
        List<BoolActionModule> actionWeaponShootLeft;

        [SerializeField]
        List<BoolActionModule> actionWeaponShootRight;

        [SerializeField]
        List<FloatActionModule> actionWeaponMoveLeft;

        [SerializeField]
        List<FloatActionModule> actionWeaponMoveRight;

        [SerializeField]
        Collider2D[] stompingTrigger;
        #endregion

        public House() {
            OnAwake += AwakeHandler;
            OnHealthChange += HealthChangeHandler;
            OnFixedUpdate += FixedUpdateHandler;
        }

        private void HealthChangeHandler(int previous, int current, int change) {
            if (current <= 0) {
                StartCoroutine(CGameOver());
            }
        }

        private IEnumerator CGameOver() {
            GameObject particles = Instantiate(gameOverExplosion);
            particles.transform.position = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
            yield return new WaitForSeconds(1);
            Camera.main.GetComponent<Blur>().enabled = true;
            gameOverUI.SetActive(true);
        }

        public void FrameMovement(LineDirection? direction) {
            // TODO test and see if proper physics work better
            Rigidbody.velocity = new Vector2(Velocity * (int)(direction == null ? 0 : direction), Rigidbody.velocity.y);
            Animator.SetInteger("Direction", direction == null ? 0 : (int)direction);
        }

        private void AwakeHandler() {
            Rigidbody = GetComponent<Rigidbody2D>();
            Animator = GetComponent<Animator>();

            SubscribeToInput(InputCrane, ActionCrane);
            SubscribeToInput(InputWeaponShootLeft, ActionWeaponShootLeft);
            SubscribeToInput(InputWeaponShootRight, ActionWeaponShootRight);
            SubscribeToInput(InputWeaponMoveLeft, ActionWeaponMoveLeft);
            SubscribeToInput(InputWeaponMoveRight, ActionWeaponMoveRight);
            SubscribeToInput(InputConveyor, ActionConveyor);
            SubscribeToInput(InputGrab, ActionGrab);
        }

        private void FixedUpdateHandler() {
            bool walkLeft = InputWalkLeft == null ? false : InputWalkLeft.Value;
            bool walkRight = InputWalkRight == null ? false : InputWalkRight.Value;

            if (walkLeft && !walkRight) {
                FrameMovement(LineDirection.Left);
            } else if (!walkLeft && walkRight) {
                FrameMovement(LineDirection.Right);
            } else {
                FrameMovement(null);
            }
        }

        private void SubscribeToInput<T, R>(InputModule<T> input, List<R> actions) where R : ActionModule<T> {
            if (input != null) {
                input.OnChange += e => InputListener(e, actions);
            }
        }

        private void InputListener<T, R>(VetoableValueChangeEvent<T> changeEvent, List<R> actions) where R : ActionModule<T> {
            foreach (ActionModule<T> action in actions) {
                if (action != null) {
                    action.IntegrateChange(changeEvent);

                    if (changeEvent.Vetoed && !changeEvent.IsRevert) {
                        changeEvent.Source.VetoEvent(changeEvent);
                        return;
                    }
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision) {
            Projectile p = collision.collider.GetComponent<Projectile>();

            if (p != null) {
                ChangeHealth(-p.Damage);
            }
        }

    }
}