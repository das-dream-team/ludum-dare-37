﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseHover : MonoBehaviour {
    [SerializeField]
    object[] tmp;
    List<Collider> objects;

    [SerializeField]
    Text description;

	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit)) {
                if(hit.collider != null) {
                    if (hit.collider.gameObject.name.Equals("Cannon_3DColl")) {
                        description.text = "Cannon: Your way to defend yourself from enemies";
                    }
                    else if (hit.collider.gameObject.name.Equals("Button_Cannon_3DColl")) {
                        description.text = "Button: You can press this button, by walking against it, to fire the Cannon";
                    } 
                    else if (hit.collider.gameObject.name.Equals("OilTank_3DColl")) {
                        description.text = "Oiltank: This is your fuel. Shooting your Cannon takes Oil and if your House takes damage it slowly regenrates using Oil. You can get oil by gathering it with your Crane.";
                    } 
                    else if (hit.collider.gameObject.name.Equals("Valve_Cannon_3DColl")) {
                        description.text = "Valve: You can turn this by using the E and Q key. This Valve controls the Cannon rotation";
                    } 
                    else if (hit.collider.gameObject.name.Equals("Valve_Crane_3DColl")) {
                        description.text = "This Valve controls the height of the crane";
                    } 
                    else if (hit.collider.gameObject.name.Equals("Button_Crane_3DColl")) {
                        description.text = "This Button activates the magnet on the crane so you can pull stuff up";
                    } 
                    else if (hit.collider.gameObject.name.Equals("Crane_3DColl")) {
                        description.text = "This Crane can be lowered, raised and can magnetically lock stuff to the platform to then move up and down";
                    } 
                    else if (hit.collider.gameObject.name.Equals("Button_Conveyor_3DColl")) {
                        description.text = "This Button extends and retracts the Conveyer Belt.";
                    }
                }
            }
        }
	}
}
