﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDamage : MonoBehaviour {

    [SerializeField]
    LD37.Entity obj;

    // Use this for initialization
    void Start () {
        StartCoroutine(damage());
	}

    IEnumerator damage() {
        yield return new WaitForSeconds(0.1f);
        obj.ChangeHealth(-199);
        obj.ChangeHealth(-1);
    }
}
