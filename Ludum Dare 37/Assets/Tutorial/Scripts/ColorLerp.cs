﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerp : MonoBehaviour {

    // Use this for initialization
    public float duration = 1;
    private Renderer rend;
    private Color originalColor;


    void Start() {
        rend = GetComponent<Renderer>();
        originalColor = rend.material.color;
    }

    void Update() {
        float lerp = Mathf.PingPong(Time.time, duration) / duration;
        //rend.material.color //= Color.Lerp(originalColor, Color.yellow, lerp);
        foreach(Material m in rend.materials) {
            m.color = Color.Lerp(originalColor, Color.yellow, lerp);
        }
    }

    


}
